# Covid Mask Usage Dataset

This repo is based on [Prajna’s awesome work](https://github.com/prajnasb/observations/).

Basically the core code is the same, just with a refactor to only include the
synthetic data creation and an extended version of the dataset.

## Dataset

The dataset is available at `data/mask_usage_dataset`. It contains a training and a test folder.
 
Training images were generated with `python loop_through_folder.py`, and hence, are synthetic images based on web-scraped images.
Those also include the images provided by Prajna. The following table include some samples from the training set:

| Faces Without Masks        | Faces With synthetic Masks   |
|----------------------------|-----------------------------------|
| ![train faces  without mask](docs/train-no-masks.jpg) | ![train faces  with mask](docs/train-synthetic-masks.jpg) |

Test images were manually labeled and are images from people actually using masks. 
The following table contains some samples from the test set:

| Faces Without Masks        | Faces With Real Masks   |
|----------------------------|-----------------------------------|
| ![test faces  without mask](docs/test-no-masks.jpg) | ![test faces  with mask](docs/test-real-masks.jpg) |   

The dataset is summarized in the following table:

|              | Train  | Test  | Total in class |
|--------------|--------|-------|----------------|
| Mask         |  1636  |  206  |      1842      |
| No Mask      |  1502  |  200  |      1702      |
| Total in set | *3138* | *406* |    **3544**    |

## How to use it

To create more samples, you must add new faces images at the  `data/faces/to_mask` directory.

Then, you must run `python loop_through_folder.py` and as a result, it will create a new folder at `data/faces/masked` 
with a new image containing a synthetic mask on it.

Masks to be added can be found at `data/images/mask_X.png`, were `X` is an integer that identifies this mask. 
Here you can see some examples:

![mask images samples](docs/masks-images.jpg)

You can also add a mask to a single image with the following command: `python mask.py suarez.jpg --mask-id 3`, 
which will result in `masked-suarez.jpg`.
 
| suarez.jpg                 | masked-suarez.jpg                 |
|----------------------------|-----------------------------------|
| ![Luis Suárez](suarez.jpg) | ![Luis Suárez](masked-suarez.jpg) |
 

You can check its usage with `python mask.py -h`

```
python mask.py -h 
usage: mask.py [-h] [--show] [--model {hog,cnn}] [--mask-id MASK_ID]
               [--dst-folder DST_FOLDER]
               pic_path

Add a face mask in the given picture. Usage example: python mask.py suarez.py
--mask-id 3

positional arguments:
  pic_path              Picture path.

optional arguments:
  -h, --help            show this help message and exit
  --show                Whether show picture with mask or not.
  --model {hog,cnn}     Which face detection model to use.
  --mask-id MASK_ID     mask id to use. Must exist a mask image at
                        data/images/mask_<mask-id>.png
  --dst-folder DST_FOLDER
                        folder where to store result image. If not given,
                        result is stored on same folder

``` 


### requirements

Run it on python 3.6.8, install requirements with `pip install -r requirements.txt`

- face-recognition==1.3.0
- face-recognition-models==0.3.0
- Pillow==7.1.2
- tqdm==4.25.0

## Model sample
You can find a sample model trained on this dataset under `model-sample/detector-mascaras.h5`. 

It's a sequential model based on [MobileNetV2](https://keras.io/api/applications/mobilenet/#mobilenetv2-function) 
architecture. 

### How to use the sample model
You must install TensorFlow 2 and OpenCV before running it.  

```python
import cv2
import numpy as np
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from tensorflow.keras.models import load_model

model = load_model('model-sample/detector-mascaras.h5')
img = cv2.imread('suarez.jpg')
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
img = cv2.resize(img, (224, 224))
img = np.expand_dims(img, 0)
img = preprocess_input(img)
score = model.predict(img)[0, 0]

print(f'model is {score*100:.2f} % sure that there is NOT a mask on image.')
```

The output should look like this:
```.env
model is 99.83 % sure that there is NOT a mask on image.
```

### Model sample performance

The following table summarizes the model performance over the given test set:

```.env
              precision    recall  f1-score   support

   with_mask      0.995     0.956     0.975       206
without_mask      0.957     0.995     0.975       200

    accuracy                          0.975       406
   macro avg      0.976     0.976     0.975       406
weighted avg      0.976     0.975     0.975       406
```

And this is the corresponding confusion matrix:

![confusion matrix](docs/sample-model-confussion-matrix.png)

More details on training & evaluation process can be found [in this notebook](https://tinyurl.com/hands-on). 
