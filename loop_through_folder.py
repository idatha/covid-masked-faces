import os
import random
from tqdm import tqdm
from mask import get_mask_path, FaceMasker

FACES_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', 'faces')
src_folder_path = os.path.join(FACES_DIR, "to_mask")
dst_folder_path = os.path.join(FACES_DIR, "masked")

os.makedirs(dst_folder_path, exist_ok=True)

images_path = [os.path.join(src_folder_path, f) for f in os.listdir(src_folder_path) if os.path.isfile(os.path.join(src_folder_path, f))]
random.shuffle(images_path)

for idx, path in enumerate(tqdm(images_path)):
    mask_path = get_mask_path(idx % 6)
    FaceMasker(path, mask_path, dst_folder=dst_folder_path).mask()
